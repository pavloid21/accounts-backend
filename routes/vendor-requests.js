import express from 'express';
import jwt from 'express-jwt';
import MYSQLConnect from '../connection/MYSQLConnection';
import config from '../config';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/', jwtCheck);
router.get('/',function (req, res) {
  MYSQLConnect.query('SELECT `tb_request`.`id`,`tb_request`.`request_status`, `tb_request`.`request_scan`, `tb_organisation`.`organisation_name`, `tb_request`.`request_date`, `tb_request_type`.`request`, `tb_systems`.`system_name`, `tb_users`.`surname`, `tb_users`.`name`, `tb_users`.`fathername` FROM `tb_request` INNER JOIN `tb_organisation` ON `tb_request`.`organisation_id` = `tb_organisation`.`id` INNER JOIN `tb_systems` ON `tb_request`.`system_id` = `tb_systems`.`id` INNER JOIN `tb_users`ON `tb_request`.`user_id` = `tb_users`.`id` INNER JOIN `tb_request_type` ON `tb_request`.`request_type_id`=`tb_request_type`.`id` ORDER BY `tb_request`.`request_date` DESC', function(error, results) {
  if (error) throw error;
    res.json(results)
})
});

router.post('/info',function (req, res) {
  MYSQLConnect.query('SELECT tb_request.id, tb_request.request_date, tb_request.request_scan, tb_request_type.request, tb_request.request_status, tb_systems.system_name, tb_organisation.organisation_name FROM tb_request INNER JOIN tb_systems ON tb_systems.id = '+[req.body.id]+' INNER JOIN tb_request_type ON tb_request_type.id = tb_request.request_type_id INNER JOIN tb_organisation ON tb_organisation.id = tb_request.organisation_id ORDER BY tb_request.request_date DESC',  function(error, results) {
  if (error) throw error;
  if(req.body.id){
    res.json(results)
  } else {
    res.send('no')
  }

})
});

export default router
