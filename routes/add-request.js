import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import MYSQLConnect from '../connection/MYSQLConnection';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/',jwtCheck);
router.post('/', function (req, res, next) {
  var sql = 'INSERT INTO tb_request (id, organisation_id, user_id, system_id, request_type_id, request_date, request_scan) VALUES (NULL,'+req.body.org+','+ req.body.user+','+req.body.sys+','+req.body.id_type+', CURRENT_DATE,"")';

  MYSQLConnect.query(sql, function(error, result){
    if (error) {
      res.send(error.stack);
    };
    if(req.body.org){
      MYSQLConnect.query('SELECT id FROM tb_request ORDER BY id DESC LIMIT 1', function(error, results){
        res.send(results);
      })

    } else {
      res.send('no')
    }
  })

})
router.use(function(err, req, res, next){
  res.status(500).json({status:err.status, message:err.message})
});


export default router
