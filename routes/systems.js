import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import MYSQLConnect from '../connection/MYSQLConnection';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/',jwtCheck);
router.get('/:id?', function (req, res, next) {
  if(req.params.id){
    Task.getTaskById(req.params.id, function(err, rows){
      if(err){
        res.json(err);
      } else {
        res.json(rows);
      }
    });
  } else {
    Task.getAllTasks(function(err, rows){
      if(err){
        res.json(err);
      } else {
        res.json(rows);
      }
    })
  }

})
router.get('/sys-name/:id?', function(req, res, next){
  if(req.params.id){
    MYSQLConnect.query('SELECT tb_module.id, tb_module.module_name FROM tb_module INNER JOIN tb_systems ON tb_module.system_id=tb_systems.id WHERE tb_systems.id=?',[req.params.id],function(err, results){
      if(err){
        res.json(err);
      } else {
        res.json(results);
      }
    })
  }
})

export default router
