import express from 'express';
import lodash from 'lodash';
import jwt from 'jsonwebtoken';
import MYSQLConnect from '../connection/MYSQLConnection';


const router = express.Router();
var secretKey = 'secretKeyIsNotForShare';

function createToken(user) {
  return jwt.sign(lodash.omit(user,'password'), secretKey, {expiresIn:60*60*5})
}
function getUserDB(username, done) {
  MYSQLConnect.query('SELECT * FROM tb_users where login=?',[username],function(err, rows,fields){
    if (err) throw err;
    done(rows[0]);
  })
}


router.post('/api/users/login', function(req, res) {
  if(!req.body.login || !req.body.password) {
    return res.status(400).send("You must send the login and password");
  }
  getUserDB(req.body.login, function(user){
    if(!user) {
      return res.status(401).send("The user is not existing");
    }
    if(user.password !== req.body.password) {
      return res.status(401).send("The login or password don't match");
    }
    res.status(201).send({
      id_token:createToken(user)
    })
  })
});

router.get('/api/users/check/:username', function(req, res){
  if(!req.params.username) {
    return res.status(400).send("You must send a login");
  }
  getUserDB(req.params.username, function(user){
    if (user) {
      res.status(201).send({id:user.id, name:user.name, surname:user.surname, fathername:user.fathername});
    } else {
      res.status(400).send("A user with that username already exists")
    }
  })
})


export default router;
