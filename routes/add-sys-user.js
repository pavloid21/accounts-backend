import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import MYSQLConnect from '../connection/MYSQLConnection';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/',jwtCheck);
router.post('/', function (req, res, next) {
  var sql = 'INSERT INTO tb_request_user (id, request_id, position, surname, name, father_name, birth_date) VALUES (NULL,'+req.body.id_req+',"'+req.body.position+'","'+ req.body.surname+'","'+req.body.name+'","'+req.body.father_name+'","'+req.body.birth_date+'")';

  MYSQLConnect.query(sql, function(error, result){
    if (error) {
      res.send(error.stack);
    };
    if(req.body.id_req){
      res.send(result);

    } else {
      res.send('no')
    }
  })

})
router.use(function(err, req, res, next){
  res.status(500).json({status:err.status, message:err.message})
});


export default router
