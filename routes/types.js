import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import MYSQLConnect from '../connection/MYSQLConnection';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/',jwtCheck);
router.get('/', function (req, res, next) {
  MYSQLConnect.query('SELECT * FROM `tb_request_type`', function(error, result){
    if (error){
      throw error
    }
    res.json(result);
  })

})


export default router
