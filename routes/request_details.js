import express from "express";
import jwt from "express-jwt";
import MYSQLConnect from "../connection/MYSQLConnection";
import config from "../config";

const router = express.Router();
var jwtCheck = jwt({
  secret: config.secretKey
});

function getDetails(id, done) {
  MYSQLConnect.query(
    "SELECT tb_request_user.id, tb_request_user.birth_date, tb_request_user.surname, tb_request_user.position,tb_request_user.name, tb_request_user.father_name, tb_respons.login, tb_respons.password FROM tb_request_user LEFT JOIN tb_respons on tb_respons.request_user_id = tb_request_user.id WHERE tb_request_user.request_id=?",
    [id],
    function(err, rows, fields) {
      if (err) throw err;
      done(rows);
    }
  );
}

router.use("/", jwtCheck);
router.get("/:id", function(req, res) {
  if (!req.params.id) {
    return res.status(400).send("You must send an id");
  }
  getDetails(req.params.id, function(details) {
    if (details) {
      res.send(details);
    }
  });
});

export default router;
