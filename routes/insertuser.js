import express from 'express';
import jwt from 'express-jwt';
import MYSQLConnect from '../connection/MYSQLConnection';
import config from '../config';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/', jwtCheck);
router.post('/',function (req, res) {
  MYSQLConnect.query('SELECT id_organisation, organisation_name FROM tb_user_organisation INNER JOIN tb_organisation ON tb_user_organisation.id_organisation=tb_organisation.id WHERE tb_user_organisation.id_user=?',[req.body.id], function(error, results) {
  if (error) throw error;
  if (req.body.id){
    res.json(results)
  } else {
    res.send('no')
  }
})
})

export default router
