import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import makeDir from 'make-dir';
import MYSQLConnect from '../connection/MYSQLConnection';


const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

String.prototype.replaceAll = function(search, replace){
  return this.split(search).join(replace);
}

router.use('/',jwtCheck);
router.post('/', (req,res,next) => {
  //console.log(req);
  let file = req.files.file;

  makeDir('files/'+req.body.org+'/'+req.body.sys+'/'+req.body.type).then(path => {
    console.log(path);
    file.mv('files/'+req.body.org+'/'+req.body.sys+'/'+req.body.type+'/'+req.body.filename, function(err){
      if (err) {
        return res.status(500).send(err);
      }
      res.json({file:'files/'+req.body.org+'/'+req.body.sys+'/'+req.body.type+'/'+req.body.filename});
      console.log(path);
      path = path.replaceAll('\\','/');
      MYSQLConnect.query('UPDATE `tb_request` SET `request_scan` = "http://192.168.1.72:8081/'+req.body.org+'/'+req.body.sys+'/'+req.body.type+'/'+req.body.filename+'" WHERE `tb_request`.`id` = '+req.body.reqid, function(err, result){
        console.log(err);
      });
    });
  })


})


export default router
