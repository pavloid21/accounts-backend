import express from 'express';
import Task from '../models/Task';
import jwt from 'express-jwt';
import config from '../config';
import MYSQLConnect from '../connection/MYSQLConnection';

const router = express.Router();
var jwtCheck = jwt({
  secret:config.secretKey
});

router.use('/',jwtCheck);
router.post('/', function (req, res, next) {
  var sql = 'INSERT INTO tb_user_mod (id, request_user_id, module_id) VALUES (NULL,'+req.body.requestuser+',"'+req.body.module+'")';

  MYSQLConnect.query(sql, function(error, result){
    if (error) {
      res.send(error.stack);
    };
    if(req.body.id_req){
      res.send(result);

    } else {
      res.send('no')
    }
  })

})
router.use(function(err, req, res, next){
  res.status(500).json({status:err.status, message:err.message})
});


export default router
