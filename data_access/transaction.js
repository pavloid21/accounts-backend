import MYSQLConnect from '../connection/MYSQLConnection'

export default function Transaction() {
  this.getAllModules = function (res) {
    MYSQLConnect.init();
    MYSQLConnect.acquire(function (err, con) {
      con.query('SELECT * FROM tb_modules', function(err, result){
        con.release();
        res.send(result);
      })
    })
  }
}
