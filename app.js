import bodyParser from 'body-parser';
import cookieParser from 'cookie-parser';
import Debug from 'debug';
import express from 'express';
import logger from 'morgan';
import path from 'path';
import cors from 'cors';
import mysql from 'mysql';
import fileUpload from 'express-fileupload';

import index from './routes/index';
import users from './routes/users';
import systems from './routes/systems';
import mo from './routes/mo';
import requests from './routes/requests';
import request_details from './routes/request_details';
import insertuser from './routes/insertuser';
import types from './routes/types';
import addrequest from './routes/add-request';
import addsysuser from './routes/add-sys-user';
import upload from './routes/upload';
import inseretusermodule from './routes/insert-user-module';
import vendorrequests from './routes/vendor-requests';
import vendorsys from './routes/vendor-system';

const app = express();
const debug = Debug('accounts-backend:app');

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(cors());
app.use(fileUpload());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));



app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
app.use(users);
app.use('/api/systems', systems);
app.use('/api/users-mo', mo);
app.use('/api/requests', requests);
app.use('/api/request_details', request_details);
app.use('/api/insertuser', insertuser);
app.use('/api/types', types);
app.use('/api/add-request', addrequest);
app.use('/api/add-sys-user', addsysuser);
app.use('/api/upload', upload);
app.use('/api/insert-user-module', inseretusermodule);
app.use('/api/vendor-requests', vendorrequests);
app.use('/api/vendor-systems', vendorsys);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
/* eslint no-unused-vars: 0 */
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  // render the error page
  res.status(err.status || 500);
  res.json(err);
});

// Handle uncaughtException
process.on('uncaughtException', (err) => {
  debug('Caught exception: %j', err);
  process.exit(1);
});

export default app;
