import MYSQLConnect from '../connection/MYSQLConnection'


var Task = {
  getAllTasks:function(callback){
    return MYSQLConnect.query("SELECT * FROM tb_systems", callback);
  },
  getTaskById:function(id, callback){
    return MYSQLConnect.query("SELECT * FROM tb_systems WHERE ID=?",[id],callback);
  },
  addTask:function(Task, callback){
    return MYSQLConnect.query("Insert into tb_systems values(?,?,?)", [Task.id,Task.system_name,Task.systym_name_short], callback);
  },
  deleteTask:function(id,callback){
    return db.query("delete from task where Id=?",[id],callback);
 },
  updateTask:function(id,Task,callback){
    return db.query("update task set Title=?,Status=? where Id=?",[Task.system_name,Task.systym_name_short,id],callback);
 }
};
export default Task;
